import "./app.css";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import { NavBar } from "./Components/Navbar/navbar";
import { LandingPage } from "./Pages/landingPage";
import { TeamsPage } from "./Pages/TeamsPage/teamsPage";

export const App = () => {
  return (
    <div className="app">
      <BrowserRouter>
        <NavBar />

        <Switch>
          <Route path="/home" component={LandingPage} />
          <Route path="/teams" component={TeamsPage} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};
