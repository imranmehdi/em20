import { Card } from "react-bootstrap";
import Moment from "moment";
import "./MatchCard.css";
import { MatchProps } from "./match";

export const Match = (matchProps: MatchProps) => {
  Moment.locale("en");

  const handleCardClick = (matchID: number) => {
    // got to match details page
    console.log("Match clicked");
  };

  return (
    <Card
      className="match-card"
      style={{
        maxWidth: "550px",
        minWidth: "500px",
        margin: "18px",
        cursor: "pointer",
      }}
      onClick={() => handleCardClick(matchProps.matchID)}
    >
      <Card.Body>
        <Card.Subtitle>
          {Moment(matchProps.matchDateTime).format("dddd, DD.MM.YYYY, HH:mm")}{" "}
          Uhr
        </Card.Subtitle>

        <Card.Title>
          <img
            src={matchProps.team1.teamIconUrl}
            alt=""
            style={{ width: "2.5rem", margin: "5px", border: "1px solid gray" }}
          />
          {matchProps.team1.teamName} : {matchProps.team2.teamName}
          <img
            src={matchProps.team2.teamIconUrl}
            alt=""
            style={{ width: "2.5rem", margin: "5px", border: "1px solid gray" }}
          />
        </Card.Title>

        <div>
          {matchProps.matchResults.map((result: any) => (
            <div key={result.resultID}>
              <div>{result.resultName}</div>
              <div>
                {result.pointsTeam1} : {result.pointsTeam2}
              </div>
            </div>
          ))}
        </div>
      </Card.Body>
    </Card>
  );
};
