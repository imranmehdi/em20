export interface MatchProps {
  matchID: number;
  matchDateTime: string;
  matchIsFinished: boolean;
  leagueName: string;
  leagueSeason: number;
  numberOfViewers: number;

  group: {
    groupName: string;
  };

  team1: {
    teamName: string;
    teamIconUrl: string;
  };

  team2: {
    teamName: string;
    teamIconUrl: string;
  };

  matchResults: [
    {
      resultName: string;
      pointsTeam1: number;
      pointsTeam2: number;
    },
    {
      resultName: string;
      pointsTeam1: number;
      pointsTeam2: number;
    }
  ];

  goals: [
    {
      goalID: number;
      scoreTeam1: number;
      scoreTeam2: number;
      matchMinute: number;
      goalGetterName: string;
      isPenalty: boolean;
      isOwnGoal: boolean;
      isOvertime: boolean;
      comment: string;
    }
  ];

  location: {
    locationCity: string;
    locationStadium: string;
  };
}
