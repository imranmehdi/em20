import { Spinner } from "react-bootstrap";

export const LoadingSpinner = () => {
  return (
    <div>
      <Spinner animation="grow" variant="success" />
      <Spinner animation="grow" variant="danger" />
      <Spinner animation="grow" variant="warning" />
    </div>
  );
};
