import { Dispatch } from "redux";

import * as teamsActions from "../../Store/TeamsStore/teamsAction";
import { getTeams } from "../../Utils/api";

export const loadTeams = () => (dispatch: Dispatch) => {
  dispatch(teamsActions.getTeamsAction());

  getTeams()
    .then((data) => {
      dispatch(teamsActions.getTeamsSuccessAction(data));
    })
    .catch((error) => dispatch(teamsActions.getTeamsFailedAction(error)));
};
