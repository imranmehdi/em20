import { Card } from "react-bootstrap";

export const Team = ({ team }: any) => {
  return (
    <Card style={{ width: "18rem", color: "black", margin: "0.5rem" }}>
      <Card.Img
        variant="top"
        src={team.teamIconUrl}
        style={{
          width: "3rem",
          margin: "5px",
          border: "1px solid gray",
        }}
      />
      <Card.Body>
        <Card.Title>
          {team.teamName} ({team.shortName})
        </Card.Title>
        <Card.Subtitle>{team.teamGroupName}</Card.Subtitle>
      </Card.Body>
    </Card>
  );
};
