import { useSelector } from "react-redux";
import { selectTeams } from "../Store/TeamsStore/teamsSelectors";

export const LandingPage = () => {
  const teams = useSelector(selectTeams);

  console.log(teams);

  return (
    <>
      <h1>Home</h1>
    </>
  );
};
