import { useEffect, useState } from "react";
import { Form, FormControl } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { LoadingSpinner } from "../../Components/LoadingSpinner/loadingSpinner";

import { Team } from "../../Components/Team/team";
import { loadTeams } from "../../Components/Team/teamsThunks";
import {
  selectTeams,
  selectTeamsLoading,
} from "../../Store/TeamsStore/teamsSelectors";
import { searchTeamsByQuery } from "./Utils/searchHelpers";

export const TeamsPage = () => {
  const style = {
    display: "flex",
    "flex-wrap": "wrap",
    "justify-content": "center",
  };

  const dispatch = useDispatch();

  const isLoading = useSelector(selectTeamsLoading);

  const teams = useSelector(selectTeams);

  const [filteredTeams, setFilteredTeams] = useState(teams);

  const [query, setQuery] = useState("");

  const handleOnSearch = (e: any) => {
    e.preventDefault();
    setQuery(e.target.value);
  };

  useEffect(() => {
    dispatch(loadTeams());
  }, []);

  useEffect(() => {
    if (teams) {
      setFilteredTeams(teams);
    }
  }, [teams]);

  useEffect(() => {
    let result = searchTeamsByQuery(teams, query);
    console.log(query, result);
    setFilteredTeams(result);
  }, [query]);

  return (
    <>
      <h1>Teams</h1>

      <div
        style={{
          margin: "5px",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Form style={{ width: "500px" }}>
          <FormControl
            type="search"
            placeholder="Search your favourite team"
            aria-label="Search"
            value={query}
            onChange={handleOnSearch}
          />
        </Form>
      </div>

      {isLoading && <LoadingSpinner />}

      <div style={style}>
        {filteredTeams.map((team: any) => (
          <Team key={team.teamId} team={team} />
        ))}
      </div>
    </>
  );
};
