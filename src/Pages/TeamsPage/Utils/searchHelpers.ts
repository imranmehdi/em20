import { ITeam } from "../../../Components/Team/teamTypes";

export const searchTeamsByQuery = (teams: ITeam[], query: string) => {
  let searchResult = teams.filter(
    (team) => team.teamName.toLowerCase().search(query.toLowerCase()) !== -1
  );

  return searchResult;
};
