import { combineReducers } from "redux";
import { teamsReducer } from "./TeamsStore/teamsReducer";

export const rootReducer = combineReducers({
  teams: teamsReducer,
});
