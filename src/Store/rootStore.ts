import { applyMiddleware, createStore } from "redux";
import { rootReducer } from "./rootReducer";
import thunk from "redux-thunk";
import { TeamsState } from "./TeamsStore/teamsReducer";

// Redux Chrome Dev Tools Extension
// https://github.com/zalmoxisus/redux-devtools-extension
// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export interface RootState {
  teams: TeamsState;
}

export const store = createStore(
  rootReducer,
  undefined,
  composeEnhancers(applyMiddleware(thunk))
);
