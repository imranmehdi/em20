import { RootState } from "../rootStore";

export const selectTeams = (state: RootState) => state.teams.teams;

export const selectTeamsLoading = (state: RootState) => state.teams.loading;

export const selectTeamByShortName =
  (shortName: string) => (state: RootState) =>
    state.teams.teams.find((team) => team.shortName === shortName);

export const selectTeamById = (id: number) => (state: RootState) =>
  state.teams.teams.find((team) => team.teamId === id);
