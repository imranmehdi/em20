import { createAction } from "typesafe-actions";
import { ITeam } from "../../Components/Team/teamTypes";

export const getTeamsAction = createAction("TEAMS_GET")();

export const getTeamsSuccessAction =
  createAction("TEAMS_GET_SUCCESS")<ITeam[]>();

export const getTeamsFailedAction = createAction("TEAMS_GET_FAILED")<any>();
