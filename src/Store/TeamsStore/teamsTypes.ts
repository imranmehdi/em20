import { ActionType } from "typesafe-actions";
import * as actions from "./teamsAction";

export type TeamsAction = ActionType<typeof actions>;
