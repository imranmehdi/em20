import { ITeam } from "../../Components/Team/teamTypes";
import { getType } from "typesafe-actions";
import * as teamsActions from "./teamsAction";
import { TeamsAction } from "./teamsTypes";

export interface TeamsState {
  teams: ITeam[];
  loading: boolean;
}

const initialState: TeamsState = {
  teams: [],
  loading: true,
};

export const teamsReducer = (
  state: TeamsState = initialState,
  action: TeamsAction
): TeamsState => {
  switch (action.type) {
    case getType(teamsActions.getTeamsAction):
      return { ...state, loading: true };

    case getType(teamsActions.getTeamsSuccessAction):
      return {
        ...state,
        teams: action.payload,
        loading: false,
      };

    case getType(teamsActions.getTeamsFailedAction):
      return {
        ...state,
        teams: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};
