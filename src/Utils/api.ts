import axios from "axios";

import { MATCHES_URL, TEAMS_URL } from "./urls";

export const getPlayer = () => {
  // TODO
};

export const getPlayers = () => {
  // TODO
};

export const getTeam = () => {
  // TODO
};

export const getTeams = () => {
  return axios
    .get(TEAMS_URL)
    .then((data) => data.data)
    .catch((error) => error);
};

export const getGroup = () => {
  // TODO
};

export const getGroups = () => {
  // TODO
};

export const getMatch = () => {
  // TODO
};

export const getMatches = () => {
  return axios
    .get(MATCHES_URL)
    .then((data) => data)
    .catch((error) => error);
};
