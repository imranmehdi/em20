export const PLAYER_URL = "";
export const PLAYERS_URL = "";

export const TEAM_URL = "";
export const TEAMS_URL =
  "https://api.openligadb.de/getavailableteams/em20/2020";

export const GROUP_URL = (groupNumber: number) =>
  `https://api.openligadb.de/getmatchdata/em20/2020/${groupNumber}`;
export const GROUPS_URL = "";

export const MATCH_URL = "";
export const MATCHES_URL = "https://api.openligadb.de/getmatchdata/em20/2020/";
